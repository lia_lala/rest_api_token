from fastapi import FastAPI, HTTPException, status
from datetime import datetime, timedelta
import jwt
from pydantic import BaseModel
from employees_data import employees
import uvicorn


app = FastAPI()

SECRET_KEY = "your_secret_key"
TOKEN_EXPIRATION_HOURS = 1


class Token(BaseModel):
    access_token: str


class User(BaseModel):
    username: str
    password: str


class SalaryInfo(BaseModel):
    salary: int
    next_promotion_date: str


def generate_token(username: str):
    expiration = datetime.utcnow() + timedelta(hours=TOKEN_EXPIRATION_HOURS)
    payload = {"username": username, "exp": expiration}
    token = jwt.encode(payload, SECRET_KEY, algorithm="HS256")
    return token


def verify_token(token: str):
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=["HS256"])
        username = payload["username"]
        expiration = payload["exp"]

        # Проверка истечения срока действия токена
        current_time = datetime.utcnow()
        if current_time > datetime.fromtimestamp(expiration):
            raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Token expired")
        return username
    except jwt.ExpiredSignatureError:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Token expired")
    except jwt.PyJWTError:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid token")

@app.post('/login', response_model=Token)
def login(user: User):
    if user.username in employees and employees[user.username]["password"] == user.password:
        token = generate_token(user.username)
        return {"access_token": token}
    else:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid username or password")


@app.get('/salary', response_model=SalaryInfo)
def get_salary(token: str):
    username = verify_token(token)
    if username in employees:
        return {"salary": employees[username]["salary"],
                "next_promotion_date": employees[username]["promotion_date"]}
    else:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Employee not found")

if __name__ == "__main__":
    uvicorn.run(app, host='127.0.0.1', port=8000)
