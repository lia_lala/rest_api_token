import pytest
from fastapi.testclient import TestClient
from main import app, generate_token


#client = TestClient(app)
@pytest.fixture()
def client():
    return TestClient(app)

@pytest.fixture()
def access_token():
    return generate_token("mike")

def test_login_success(client):
    response = client.post("/login", json={"username": "mike", "password": "qwerty123"})
    assert response.status_code == 200
    assert "access_token" in response.json()

def test_login_failure(client):
    response = client.post("/login", json={"username": "mike", "password": "invalid"})
    assert response.status_code == 401

def test_get_salary_success(client, access_token):
    response = client.get("/salary", params={"token": access_token})
    assert response.status_code == 200
    assert "salary" in response.json()
    assert "next_promotion_date" in response.json()


def test_get_salary_unauthorized(client):
    response = client.get("/salary")
    assert response.status_code == 422

def test_get_salary_invalid_token(client):
    response = client.get("/salary", headers={"Authorization": "Bearer invalid_token"})
    assert response.status_code == 422
